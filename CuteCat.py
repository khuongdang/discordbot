import discord
import Funtion 
from Protected import Variant
from discord.ext import commands
bot = commands.Bot(command_prefix='cat.', description='A bot that greets the user back.')


@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')

@bot.command()
async def greet(ctx):
    await ctx.send(":smiley: :wave: Meoww! https://media.giphy.com/media/JIX9t2j0ZTN9S/giphy.gif")

@bot.command()
async def info(ctx):
    await ctx.send(embed= Funtion.get_info() )

bot.remove_command('help')

@bot.command()
async def help(ctx):
    await ctx.send(embed= Funtion.get_help())

bot.run(Variant.token)
