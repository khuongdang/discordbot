import discord

appTitle = "CUTE CAT"
prefix = "cat."

def get_info():
    embed = discord.Embed(title=appTitle, description="Cutest cat there is ever.", color=0xeee657)
    
    # give info about you here
    embed.add_field(name="Author", value="OOpIce")
    
    # Shows the number of servers the bot is member of.
    #embed.add_field(name="Server count", value=" null ")

    # give users a link to invite thsi bot to their server
    #embed.add_field(name="Invite", value=" null ")

    return embed

def get_help():
	embed = discord.Embed(title=appTitle, description="A Very Cute cat. List of commands are:", color=0xeee657)

	embed.add_field(name=prefix+"greet", value="Gives a nice Meowwww", inline=False)
	embed.add_field(name=prefix+"info", value="Gives a little info about the cat", inline=True)
	embed.add_field(name=prefix+"help", value="Gives this message", inline=False)
	return embed